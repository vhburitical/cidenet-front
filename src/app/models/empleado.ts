export class Empleado{
    id!: number; 
	surname!: string;
	secondSurname!: string;
	firstName!:string;
	secondName!:string;
	countryEmployment!:string;
	identificationType!:string;
	identificationNumber!:string;
	email!:string;
	area!:string;
	status!:string;	
	admissionDate!:Date;
    registrationDate!: string;
}