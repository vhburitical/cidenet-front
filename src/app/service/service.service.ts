import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empleado } from '../models/empleado';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private urlEndPoint: string = 'http://localhost:8080/api/clientes';

  constructor(private http: HttpClient) { }
  getEmpleados(): Observable<any> {
    return this.http.get(this.urlEndPoint);
  }
  postCreate(empleado: Empleado) {
    return this.http.post(this.urlEndPoint, empleado);
  }
  update(data: Empleado): Observable<Empleado> {
    return this.http.put<Empleado>(`${this.urlEndPoint}/${data.id}`, data);
  }
  findById(id: number): Observable<Empleado> {
    return this.http.get<Empleado>(`${this.urlEndPoint}/${id}`);
  }
  delete(id: number): Observable<Empleado> {
    return this.http.delete<Empleado>(`${this.urlEndPoint}/${id}`)
  }
}
