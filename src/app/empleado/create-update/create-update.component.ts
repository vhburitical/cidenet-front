import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Empleado } from '../../models/empleado';
import { ServiceService } from '../../service/service.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.css']
})
export class CreateUpdateComponent implements OnInit {
  identificationType: any[] = ['Cédula', 'Cédula de Extranjería', 'Pasaporte', 'Permiso Especial'];
  countryEmployment: any[] = ['Colombia', 'Estados Unidos'];
  area: any[] = ['Administración', 'Financiera', 'Compras', 'Infraestructura', 'Operación', 'Talento Humano', 'Servicios Varios'];
  accion = 'Crear';
  id: any;
  empleado: any;

  form: FormGroup;
  maxDate: Date;

  constructor(private fb: FormBuilder,
    private service: ServiceService,
    private snackBar: MatSnackBar,
    private router: Router,
    private params: ActivatedRoute
  ) {
    this.empleado = new Empleado;
    const idParam = 'id';
    this.id = this.params.snapshot.params[idParam];
  }

  ngOnInit(): void {


    if (this.id !== undefined) {
      this.accion = 'Editar';
      this.esEditar();
    }
    this.inicializarForm();
  }
  /**
   * Method to initialize the form
   */
  inicializarForm() {
    this.form = this.fb.group({
      firstName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('[a-zA-Z ]*')]],
      secondName: ['', [Validators.maxLength(20), Validators.pattern('[a-zA-Z ]*')]],
      surname: ['', [Validators.pattern('[a-zA-Z ]*')]],
      secondSurname: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('[a-zA-Z ]*')]],
      countryEmployment: [''],
      identificationType: [''],
      identificationNumber: ['', [Validators.required, Validators.pattern('[0-9 ]*')]],
      area: [''],
      email: [{ value: '', disabled: true }],
      registrationDate: [{ value: '', disabled: true }],
      state: [{ value: '', disabled: true }],
      addmissionDate: ['', [Validators.required]]
    })

  }
  /**
   * 
   */
  saveEmployee() {
    const empleado: Empleado = {
      id: this.id,
      surname: this.form.get("surname")?.value.toUpperCase(),
      secondSurname: this.form.get("secondSurname")?.value.toUpperCase(),
      firstName: this.form.get("firstName").value.toUpperCase(),
      secondName: this.form.get("secondName").value.toUpperCase(),
      countryEmployment: this.form.get("countryEmployment")?.value.toUpperCase(),
      identificationType: this.form.get("identificationType")?.value.toUpperCase(),
      identificationNumber: this.form.get("identificationNumber")?.value.toUpperCase(),
      email: this.form.get("email")?.value.toUpperCase(),
      area: this.form.get("area")?.value.toUpperCase(),
      status: this.form.get("status")?.value.toUpperCase(),
      admissionDate: this.form.get("admissionDate")?.value,
      registrationDate: this.form.get("registrationDate")?.value
    };
    if (this.id !== undefined) {

      this.updateEmpleado(empleado);
    } else {
      this.crearEmpleado(empleado);
    }

  }
  /**
   * Method to create an employee
   * @param empleado 
   */
  crearEmpleado(empleado: Empleado) {
    this.service.postCreate(empleado).subscribe((data) => {
      if (data != null) {
        swal.fire('Nuevo Empleado', 'se ha creado con éxito', 'success');
        this.router.navigate(['/']);
      } else {
        this.snackBar.open('Revisa el documento', '', {
          duration: 3000
        });
      }

    });

  }
  /**
   * Method to update an employee
   * @param empleado 
   */
  updateEmpleado(empleado: Empleado) {
    this.service.update(empleado).subscribe((data) => {
      if (data) {
        this.snackBar.open('El empleado fue actualizado con exito!', '', {
          duration: 3000
        });
        this.router.navigate(['/']);
      } else {
        this.snackBar.open('Revisa el documento', '', {
          duration: 3000
        });
      }
    })

  }
  /**
   * Method that fills in the fields of the form when it is to be updated
   */
  esEditar() {
    this.service.findById(this.id).subscribe(data => {
      this.empleado = data;
      this.form.patchValue({
        surname: this.empleado.surname,
        secondSurname: this.empleado.secondSurname,
        firstName: this.empleado.firstName,
        secondName: this.empleado.secondName,
        countryEmployment: this.empleado.countryEmployment,
        identificationType: this.empleado.identificationType,
        identificationNumber: this.empleado.identificationNumber,
        email: this.empleado.email,
        area: this.empleado.area,
        status: this.empleado.status,
        admissionDate: this.empleado.admissionDate,
        registrationDate: this.empleado.registrationDate
      })
    })
  }
  /**
   * Method to return to initial list
   */
  cancelar() {
    this.router.navigate(['/']);
  }
}
