import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './empleado/listar/listar.component';
import { CreateUpdateComponent } from './empleado/create-update/create-update.component';

const routes: Routes = [
  { path: '', component: ListarComponent },
  { path: 'add', component: CreateUpdateComponent },
  { path: 'edit/:id', component: CreateUpdateComponent },
  { path: '**', component: ListarComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
