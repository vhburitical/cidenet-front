import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service/service.service';
import { MatTableDataSource } from '@angular/material/table';
import swal from 'sweetalert2';
import { Empleado } from '../../models/empleado';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  listColumn: string[] = ['firstName', 'secondName', 'surname', 'secondSurname', 'identificationType', 'identificationNumber', 'countryEmployment', 'email', 'status', 'actions'];
  listEmpleados: any[] = [];
  dataSource = new MatTableDataSource(this.listEmpleados);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private service: ServiceService) { }

  ngOnInit(): void {
    this.getEmpleado();
    
  }
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }
  
  /**
   * Method to list all employees in table
   */
  getEmpleado() {
    this.service.getEmpleados().subscribe(data => {
      this.listEmpleados = data;
      this.dataSource = new MatTableDataSource(this.listEmpleados);
    })

  }
  /**
   * Method to apply the filter on the table
   * @param event 
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  /**
   * Method to remove an employee
   * @param id 
   */
  delete(id: number) {
    swal.fire({
      title: 'Está seguro?',
      text: `¿ Seguro que desea eliminar al empleado?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.service.delete(id).subscribe(
          resp => {
            if (resp) {
              swal.fire('Empleado', 'se ha eliminado con éxito', 'success');
              this.getEmpleado();
            } else {
              swal.fire('Empleado', 'no existe', 'success');
              this.getEmpleado();
            }

          });
      }
    });

  }

}
